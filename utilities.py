# -*- coding: utf-8 -*-
"""
:synopsis: Common Utility General Classes for Python.

It defines the following classes and functions:
    :class:`Logger`

:created:    2014/11/27

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1
:updated:    2014-11-27
"""

#===============================================================================
# Imports
#===============================================================================
from collections import defaultdict


#===============================================================================
# Global variables
#===============================================================================


#===============================================================================
# Mixins definitions
#===============================================================================


#===============================================================================
# Decorators definitions
#===============================================================================


#===============================================================================
# Class definitions
#===============================================================================
class Logger(object):
    def __init__(self, key_func=None, *func_args, **func_kwargs):
        self._log = defaultdict(list)
        self._crono_keys = list() #List of keys in self._log, in chronological order
        
        if not key_func:
            self.key_func = self.crono_index
        else:
            self.key_func = key_func
        
        self.func_args = func_args
        self.func_kwargs = func_kwargs
        
    def __call__(self, key=None):
        if key:
            return list( self._log.get(key, []) )
        else:
            return self.log
    
    def get_log(self):
        return self._log.copy()
    def set_log(self, value):
        self.put(value)
    def del_log(self):
        self._log.clear()
        self._crono_keys = list()
    log = property(get_log, set_log, del_log)
    
    def put(self, value):
        key = self.key_func(*self.func_args, **self.func_kwargs)
        self.put_with_key(key, value)
        return key
        
    def put_with_key(self, key, value):
        self._log[key].append(value)
        self._crono_keys.append(key)
        
    def keys(self):
        return list(self._crono_keys)
    
    def crono_index(self, start=0):
        return len(self._crono_keys) + start


#===============================================================================
# Functions declarations
#===============================================================================





if __name__ == '__main__':
    pass