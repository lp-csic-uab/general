# -*- coding: utf-8 -*-

"""
:synopsis: Common Basic General Classes for Python.

:created:    2013/07/04

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.3'
__UPDATED__ = '2018-08-06'

#===============================================================================
# Imports
#===============================================================================
import functools
import re
import pickle, json, csv, os, shutil
from collections import Mapping, Hashable

from basic_func import group_by_attrs


#===============================================================================
# Mixins definitions
#===============================================================================
class InitKwargs2AttrMixin(object):
    """
    Automatically set the attributes of an instance from the keyword arguments
    passed during initialization. Recommendation: put it in the last position
    in the inheritance chain, because it doen't call super in :method:`__init__`.
    """
    def __init__(self, **kwargs):
        for kwarg, value in kwargs.items():
            if not callable(value):
                setattr(self, kwarg, value)
            else:
                setattr( self, kwarg, value.__get__(self) )


class ReprMixin(object):
    """
    Mixin to add a standardized :method:`__repr__`
    """
    __repr_init_attr__ = tuple()
    
    def __init__(self, *args, **kwargs):
        # X_NOTE: this is only needed if it's not the last superclass in the Method Resolution Order chain of classes
        super(ReprMixin, self).__init__(*args, **kwargs)
    
    def __repr__(self):
        init_kwargs = ", ".join("=".join(( attr, repr( getattr(self, attr) ) )) 
                                for attr in self.__repr_init_attr__)
        return "{0}({1})".format(self.__class__.__name__, init_kwargs)


class EqMixin(object):
    """
    Mixin to add a standardized :method:`__eq__`
    Inspired by http://stackoverflow.com/a/2909119
    """
    __eq_attr__ = tuple()
    
    def __eq_attr_values__(self):
        return tuple(getattr(self, attr) for attr in self.__eq_attr__)
    
    def __eq__(self, othr):
#         return isinstance(othr, self.__class__) and self.__eq_attr_values__() == othr.__eq_attr_values__() #CAUTION: This can be dangerous if subclasses redefine :method:`__eq_attr_values__` or :attr:`__eq_attrs__`
        return othr.__class__ == self.__class__ and self.__eq_attr_values__() == othr.__eq_attr_values__()


class HashEqMixin(EqMixin):
    """
    Mixin to add a standardized :method:`__hash__` along a standardized
    :method:`__eq__`
    Inspired by http://stackoverflow.com/a/2909119
    """
    def __hash__(self):
        return hash( self.__eq_attr_values__() )
#         # More complex implementation using a xor between attribute value hashes.
#         # Inspired by http://stackoverflow.com/a/19073010:
#         eq_vals = self.__eq_attr_values__()
#         return reduce( lambda x,y: x^hash(y), eq_vals[1:], hash( eq_vals[0] ) )


#===============================================================================
# Decorators definitions
#===============================================================================
class Memoized(object):
    """
    A memoize wrapper/decorator that generates a memoized (with results cached)
    coroutine (if *fixed_args or *fixed_kwargs supplied during initialization)
    of a function or callable
    
    With ideas from https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
    """
    
    def __init__(self, a_callable, cache_storage = None, *fixed_args, **fixed_kwargs):
        """
        :param callable a_callable:
        :param dict cache_storage: optional dictionary-like object to store the cached data in
        """
        if cache_storage is None:
            self.external_cache = False
            cache_storage = a_callable.__memoize_cache__ = dict()
        else:
            self.external_cache = True
        self.default_cache = cache_storage
        
        self.callable = a_callable
        self.__doc__ = a_callable.__doc__
        self.fixed_argskwargs = (fixed_args, fixed_kwargs)
        
    def __call__(self, *args, **kwargs):
        memoize_obj = kwargs.pop( '__memoize_obj__', tuple() ) #Get the memoize object, if passed. Also avoids storing object in the memoize cache deleting it from the ``kwargs``
        cache = kwargs.pop( '__memoize_cache__', self.default_cache ) #Get the cache dictionary to use, if passed, otherwise use the default one.
        # Build a cache_key from the passed ``args`` and ``kwargs``:
        sorted_kwargs = sorted( kwargs.keys() )
        cache_key = args + tuple(sorted_kwargs) + tuple( (kwargs[key] for key in sorted_kwargs) )
        if not all( (isinstance(arg, Hashable) for arg in cache_key) ):
            cache_key = repr(cache_key)
        # If the cache_key is not in the cache get the result from the function
        # (taking into account fixed args, fixed kwargs and the object to pass
        # as ``self`` to a method), and store it on the cache:
        if cache_key not in cache:
            fixed_args, fixed_kwargs = self.fixed_argskwargs
            args = memoize_obj + fixed_args + args[len(fixed_args):]
            kwargs.update(fixed_kwargs)
            cache[cache_key] = self.callable(*args, **kwargs)
        # Return the cached result:
        return cache[cache_key]
    
    def __repr__(self):
        return "<Memoized %r>"%self.callable
    
    def __get__(self, obj, cls):
        """Support instance and class methods through the descriptor protocol"""
        if not self.external_cache:
            if obj is not None: #Instance method call:
                if not hasattr(obj, '__memoize_cache__'): #First time use of memoized instance method
                    obj.__memoize_cache__ = dict()
                cache = obj.__memoize_cache__.setdefault( self.callable.func_name, dict() ) #Get or initialize the cache for the current memoized method (allows independent caches for other memoized methods)
            else: #Class method call:
                cache = self.default_cache
        return functools.partial( self.__call__, __memoize_obj__=(obj,), __memoize_cache__=cache)


#===============================================================================
# Class definitions
#===============================================================================
class BasicContainer(Mapping):
    """
    A dictionary-like class. Contains elements that can be indexed by some
    attribute and accessed by this attribute values like a read-only
    dictionary.
    """
    def __init__(self, elements=None, element_cls=None, element_key=None, flat=True):
        self._need_refresh_view = False
        self._view = None
        self._elements = set()
        self._element_cls = element_cls
        self._element_key = None
        self.element_key = element_key
        if elements:
            self.extend(elements)
        self.flat = flat
    
    def __getitem__(self, key):
        if self._need_refresh_view:
            self.refresh_view()

        if self._element_key:
            view_value = self._view[key]
            if self.flat:
                view_value = view_value[0]
            return view_value
        else:
            raise KeyError(key)
    
    def __call__(self, key):
        return self.__getitem__(key)
    
    def __iter__(self):
        return iter(self._elements)
    
    def __len__(self):
        return len(self._elements)

    @property
    def element_key(self):
        return self._element_key
    
    @element_key.setter
    def element_key(self, key):
        if key == self._element_key:
            return
        #
        if self._element_cls:
            try:
                test_obj = self._element_cls()
            except TypeError:
                test_obj = self._element_cls
            if not hasattr(test_obj, key):
                raise ValueError("Objects of type {0} have no '{1}' attribute,"\
                                 " so '{1}' cannot be used as an "\
                                 "elemnet_key".format(type(test_obj), key)
                                 )
        #
        self._element_key = key
        self._need_refresh_view = True
    
    @property
    def element_cls(self):
        return self._element_cls
    
    def extend(self, elements):
        for element in elements:
            self.add(element)
        return self
    
    def add(self, element):
        if self._element_cls and not isinstance(element, self._element_cls):
            raise TypeError( "{0} object is not a '{1}', so it cannot be added"\
                             " to {2}".format(type(element), 
                                              repr(self._element_cls), 
                                              repr(self)) 
                            )
        #
        self._elements.add(element)
        self._need_refresh_view = True
        return self
    
    def refres_view(self):
#         view = collections.defaultdict(set)
#         for element in self._elements:
#             view[ getattr(element, self._element_key, None) ].add(element)
        self._view = group_by_attrs(self._elements, self._element_key)
        self._need_refresh_view = False
        return self._view


class ObjectDict(dict):
    """
    A dictionary whose keys can be accessed as object attributes. The instanced
    objects behave like JavaScript objects.
    """
    
    _restricted_keys = tuple(dict.__dict__.keys()) + ('_restricted_keys', '_sanitizekey')

    def __setitem__(self, key, value):
        return dict.__setitem__(self, self._sanitizekey(key), value)
    
    def __setattr__(self, attr, value):
        return self.__setitem__(attr, value)
    
    def __getattr__(self, attr):
        return self.__getitem__(attr)
    
    def _sanitizekey(self, key):
        if not isinstance(key, str):
            key = str(key)
        if key in self._restricted_keys or key[0].isdigit():
            key = '_' + key
        key = re.sub('\W', '_', key)
        return key


class PersistentDict(dict):
    """
    Persistent dictionary with an API compatible with shelve and anydbm.
    ( http://code.activestate.com/recipes/576642/ )
    
    The dict is kept in memory, so the dictionary operations run as fast as a
    regular dictionary.
    
    Write to disk is delayed until close or sync (similar to gdbm's fast mode).
    
    Input file format is automatically discovered.
    Output file format is selectable between pickle, json, and csv. All three
    serialization formats are backed by fast C implementations.
    
    (c) Created by Raymond Hettinger on Wed, 4 Feb 2009 (MIT license) 
    """
    
    def __init__(self, filename, flag='c', mode=None, format='pickle', *args, **kwds):
        self.flag = flag                    # r=readonly, c=create/continue, or n=new
        self.mode = mode                    # None or an octal triple like 0644
        self.format = format                # 'csv', 'json', or 'pickle'
        self.filename = filename
        if flag != 'n' and os.access(filename, os.R_OK):
            fileobj = open(filename, 'rb' if format=='pickle' else 'r')
            with fileobj:
                self.load(fileobj)
        dict.__init__(self, *args, **kwds)
    
    def sync(self):
        'Write dict to disk'
        if self.flag == 'r':
            return
        filename = self.filename
        tempname = filename + '.tmp'
        fileobj = open(tempname, 'wb' if self.format=='pickle' else 'w')
        try:
            self.dump(fileobj)
        except Exception:
            os.remove(tempname)
            raise
        finally:
            fileobj.close()
        shutil.move(tempname, self.filename)    # atomic commit
        if self.mode is not None:
            os.chmod(self.filename, self.mode)
    
    def close(self):
        self.sync()
    
    def __enter__(self):
        return self
    
    def __exit__(self, *exc_info):
        self.close()
    
    def dump(self, fileobj):
        if self.format == 'csv':
            csv.writer(fileobj).writerows(self.items())
        elif self.format == 'json':
            json.dump(self, fileobj, separators=(',', ':'))
        elif self.format == 'pickle':
            pickle.dump(dict(self), fileobj, 2)
        else:
            raise NotImplementedError('Unknown format: ' + repr(self.format))
    
    def load(self, fileobj):
        # Try formats from most restrictive to least restrictive
        for loader in (pickle.load, json.load, csv.reader):
            fileobj.seek(0)
            try:
                return self.update(loader(fileobj))
            except Exception:
                pass
        raise ValueError('File not in a supported format')


class ReadOnlyDict(dict):
    """
    Read-only dictionary class.
    
    Modified from http://stackoverflow.com/a/19023331
    """
    
    __readonly = True
    
    def readonly(self, allow=True):
        """Deny or Allow modifying dictionary"""
        self.__readonly = bool(allow)
    
    def readandwrite(self, allow=True):
        """Allow or Deny modifying dictionary"""
        self.readonly(not bool(allow))
    
    def __setitem__(self, key, value):
        if self.__readonly:
            raise TypeError, "dictionary in read-only mode doesn't support __setitem__"
        return dict.__setitem__(self, key, value)
    
    def __delitem__(self, key):
        if self.__readonly:
            raise TypeError, "dictionary in read-only mode doesn't support __delitem__"
        return dict.__delitem__(self, key)


class PseudoStructMeta(type):
    """
    Metaclass to ensure PseudoStruct subclasses get a full __allslots__ 
    initialized with all the slots, including those inherited from 
    super classes
    """
    def __new__(cls, name, bases, attrs):
        new_cls = super(PseudoStructMeta, cls).__new__(cls, name, bases, attrs)
        # Initialize new class `__allslots__` class attribute with all the 
        # slots, including those inherited from super classes:
        allslots = set() #Use a set to avoid duplicates.
        for scls in reversed(new_cls.__mro__): #Traverse the class inheritance tree from parent to child
            scls_slots = getattr( scls, "__slots__", tuple() )
            if isinstance(scls_slots, str): #Only one slot:
                allslots.add(scls_slots)
            else: #Many slots:
                allslots.update(scls_slots)
        new_cls.__allslots__ = tuple(allslots)
        return new_cls


class PseudoStruct(object):
    """
    Modified from http://code.activestate.com/recipes/578349-pseudostruct/ , 
    and with some ideas from http://code.activestate.com/recipes/502237-simple-record-aka-struct-type/
    
    A fast, small-footprint, structure-like type suitable for use as
    a data transfer or parameter object.
    This class is not intended to be used directly. Subclasses should
    define the fields they care about as slots.
    """
    __metaclass__ = PseudoStructMeta
    __allslots__ = tuple() # All the slots, including those inherited from super-classes. It's filled by the metaclass.

    __slots__ = tuple() # Subclasses should override this class attribute.
    
    @classmethod
    def define(cls, class_name, *field_names):
        """Dynamically create a class definition for a subclass of
        ``PseudoStruct``.

        Note that objects of a dynamically created class cannot be
        pickled. If pickling support is needed, the subclass must have
        a static definition.

        >>> X = PseudoStruct.define('NewClass', 'attr1', 'attr2')
        >>> X.__name__
        'NewClass'
        >>> issubclass(X, PseudoStruct)
        True
        >>> X.__slots__
        ('attr1', 'attr2')
        """
        return type(class_name, (cls,), {"__slots__": field_names})
    
    def __init__(self, *args, **kwargs):
        if args:
            for attr, arg in zip(self.__allslots__, args):
                setattr(self, attr, arg)
        if kwargs:
            self.__setstate__(kwargs)
    
    def __getattr__(self, attr):
        """Lazily initialize the value for ``attr``.

        >>> class Attribute(PseudoStruct):
        ...     __slots__ = ['attr', 'value']
        ...
        >>> attr = Attribute()
        >>> attr.attr is None
        True
        >>> attr.description is None
        Traceback (most recent call last):
        ...
        AttributeError: 'Attribute' object has no attribute 'description'
        """
        # only called once, if a value has not yet been assigned to the slot;
        # returns None as a reasonable default; if the attr is not a slot,
        # raises AttributeError as expected
        setattr(self, attr, None)
        
    def __getstate__(self):
        r"""Build the state object for pickling this object.

        >>> class Attribute(PseudoStruct):
        ...     __slots__ = ['name', 'value']
        ...
        >>> attr = Attribute()
        >>> attr.name = 'test'
        >>> sorted(attr.__getstate__().items())
        [('name', 'test'), ('value', None)]
        """
        return { slot: getattr(self, slot) for slot in self.__allslots__ }
    
    def __setstate__(self, state):
        """
        Initialize this object from a pickling state object.

        >>> class Attribute(PseudoStruct):
        ...     __slots__ = ['name', 'value']
        ...
        >>> attr = Attribute()
        >>> attr.name = 'test'
        >>> state = attr.__getstate__()
        >>> attr2 = Attribute()
        >>> attr2.__setstate__(state)
        >>> attr2.name == 'test'
        True
        >>> attr2.value is None
        True
        """
        for name, value in state.items():
            setattr(self, name, value)
    
    def __iter__(self):
        """
        Iterator method
        """
        for slot in self.__allslots__:
            yield getattr(self, slot)
    
    def __eq__(self, other):
        """
        :return: True if the internal states of *other* and self are
        equal.

        >>> class Sample(PseudoStruct):
        ...     __slots__ = ['value']
        ...
        >>> sample1 = Sample()
        >>> sample2 = Sample()
        >>> sample1 == sample2
        True
        >>> sample2.value = 'test'
        >>> sample1 == sample2
        False
        """
        return (self.__getstate__() == other.__getstate__())
    
    def __hash__(self):
        """
        :return: the hash of this pseudo-structure's internal state.

        >>> class Sample(PseudoStruct):
        ...     __slots__ = ['value']
        ...
        >>> sample1 = Sample()
        >>> sample2 = Sample()
        >>> hash(sample1) == hash(sample2)
        True
        >>> sample2.value = 'test'
        >>> hash(sample1) == hash(sample2)
        False
        """
        return hash( tuple( sorted(self.__getstate__().items()) ) )
    
    def __repr__(self):
        """
        :return: an unambiguous description of this object.

        >>> class Sample(PseudoStruct):
        ...     __slots__ = ['value']
        ...
        >>> sample = Sample()
        >>> repr(sample)
        "Sample(value=None)"
        """
        return "%s(%s)" % (self.__class__.__name__, 
                           ', '.join( '%s=%r' % tup 
                                      for tup in self.__getstate__().items() )
                           )


if __name__ == '__main__':
    od = ObjectDict(a=1, b=2)
    od.a