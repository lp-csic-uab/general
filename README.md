---

**WARNING!**: This is the *Old* source-code repository for General shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/general_code/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/general_code/**  

---  
  
  
# General shared Package

General python 2.7.x code to reuse in different projects  


---

**WARNING!**: This is the *Old* source-code repository for General shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/general_code/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/general_code/**  

---  
  